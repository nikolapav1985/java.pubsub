import java.util.List;
import java.util.ArrayList;

public class Item extends Publisher{ // can be any publisher
    public List<Subscriber> sub=new ArrayList<>();
    public void note() // notify each subscriber 
    {
        int i=0;
        for(;i<sub.size();i++){
            sub.get(i).react();
        }
    }
    public void addSubscriber(Subscriber subscriber) // add a subscriber 
    {
        sub.add(subscriber);
    }
}
