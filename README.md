Publisher subscriber pattern example
------------------------------------

- Publisher.java (abstract class, can be any publisher)
- Subscriber.java (abstract class)
- Item.java (example publisher)
- Buyer.java (example subscriber)
- Main.java (run example)

Compile
-------

- javac Main.java

Run
---

- java Main.java

Test environment
----------------

- javac version 14.0.2
- os lubuntu 16.04 lts
