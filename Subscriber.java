abstract class Subscriber{ // a subscriber
    public abstract void react(); // react to some event
}
