public class Main{
    public static void main(String []args){
        Publisher basketball=new Item();
        Subscriber buyera=new Buyer();
        Subscriber buyerb=new Buyer();
        Subscriber buyerc=new Buyer();

        basketball.addSubscriber(buyera);
        basketball.addSubscriber(buyerb);
        basketball.addSubscriber(buyerc);
        basketball.note();
    }
}
