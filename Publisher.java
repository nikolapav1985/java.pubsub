abstract class Publisher{ // can be any publisher
    public abstract void note(); // notify each subscriber
    public abstract void addSubscriber(Subscriber subscriber); // add a subscriber
}
